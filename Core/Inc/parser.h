/*
 * parser.h
 *
 *  Created on: Aug 22, 2020
 *      Author: Mateusz Salamon
 */

#ifndef INC_PARSER_H_
#define INC_PARSER_H_
#include "uartdma.h"


#define IloscRozkazowData 2
#define IloscPolecenData 3
#define DlugoscRozkazuData 15

#define IloscRozkazowCzas 2
#define IloscPolecenCzas 3
#define DlugoscRozkazuCzas 15

typedef struct {
	char polecenie[16];  // polecenie
	char *rozkaz[IloscRozkazowData][DlugoscRozkazuData];  // rozkaz czyli wartości jakię mogą być użyte w poleceniu np "on", "off", "d1-10"
} T_POLECENIE;


void UART1_ParseLine(UARTDMA_HandleTypeDef *huartdma);
void UART2_ParseLine(UARTDMA_HandleTypeDef *huartdma);
void weryfikujDate( char bufor[120] );
void weryfikujTime( char bufor[120] );

#endif /* INC_PARSER_H_ */
