/*
 * parser.c
 *
 *  Created on: Aug 22, 2020
 *      Author: Mateusz Salamon
 */
#include "main.h"
#include "parser.h"
#include "string.h"
#include "uartdma.h"
#include "stdlib.h"
#include "stdio.h"
#include "rtc.h"

extern UARTDMA_HandleTypeDef huartdma2;
extern UARTDMA_HandleTypeDef huartdma1;

extern RTC_TimeTypeDef RtcTime;
extern RTC_DateTypeDef RtcDate;


// struktura do walidacji daty
T_POLECENIE poleceniaDATA[IloscPolecenData] = {
    {"ROK", {"d0-99"}}, // rok może zawierać tylko cyfry a wartości z zakresu 0 - 99
    {"MC", {"d1-12"}},  // miesiąc może zawierać tylko cyfry a wartości z 1 -12
    {"DZ", {"d1-31"}},  // dzień tylko cyfry z zakresu 1 - 31
};

T_POLECENIE poleceniaTIME[IloscPolecenCzas] = {
    {"GG", {"d0-23"}},  // godzimy
    {"MM", {"d0-59"}},  // minuty
    {"SS", {"d0-59"}},  // sekundy
};


char Message[64]; // Transmit buffer
char MyName[32] = {"No Name"}; // Name string
uint8_t MessageLen;

/*
 * 		LED=1\n 	// LED On
 * 		LED=0\n 	// LED Off
 */
void UART_ParseLED()
{
	uint8_t LedState; // Received state variable

	char* ParsePointer = strtok(NULL, ","); // Look for next token or end of string
	// Should be now: ParsePointer == 1'\0'

	if(strlen(ParsePointer) > 0) // If string exists
	{
		if(ParsePointer[0] < '0' || ParsePointer[0] > '9') // Chceck if there are only numbers
		{
			UARTDMA_Print(&huartdma2, "LED wrong value. Don't use letters dude!\r\n"); // Print message
			return;	// And exit parsing
		}

		LedState = atoi(ParsePointer); // If there are no chars, change string to integer

		if(LedState == 1) // LED ON
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
			//UARTDMA_Print(&huartdma2, "LED On\r\n");
			UARTDMA_Print(&huartdma1, "LED On\r\n");
		}
		else if(LedState == 0) // LED OFF
		{
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
			//UARTDMA_Print(&huartdma2, "LED Off\r\n");
			UARTDMA_Print(&huartdma1, "LED Off\r\n");
		}
		else // Wrong state number
		{
			UARTDMA_Print(&huartdma2, "LED wrong value. Use 0 or 1.\r\n");
		}
	}
}

/*
 * ENV=X,Y,Z\0 // X - temperature, Y - humidity, Z - pressure
 */
void UART_ParseENV()
{
	uint8_t i,j; // Iterators
	float EnvParameters[3]; // Temperature, humidity, pressure

	for(i = 0; i<3; i++) // 3 parameters are expected
	{
		char* ParsePointer = strtok(NULL, ","); // Look for next token or end of string

		if(strlen(ParsePointer) > 0) // If string exists
		{
			for(j=0; ParsePointer[j] != 0; j++) // Loop over all chars in current strong-block
			{
				if((ParsePointer[j] < '0' || ParsePointer[j] > '9') && ParsePointer[j] != '.' ) // Check if there are only numbers or dot sign
				{
					sprintf(Message, "ENV wrong value. Don't use letters dude!\r\n"); // If not, Error message
					UARTDMA_Print(&huartdma2, Message); // Print message
					return;	// And exit parsing
				}

				EnvParameters[i] = atof(ParsePointer); // If there are no chars, change string to integer
			}
		}
		else
		{
			sprintf(Message, "ENV too less values. ENV=X,Y,Z\\n\r\n"); // If not, Error message
			UARTDMA_Print(&huartdma2, Message); // Print message
			return;	// And exit parsing
		}
	}

	// Print back received data
	sprintf(Message, "Temperature: %.1f\r\n", EnvParameters[0]);
	UARTDMA_Print(&huartdma2, Message);

	sprintf(Message, "Humidity: %.1f\r\n", EnvParameters[1]);
	UARTDMA_Print(&huartdma2, Message);

	sprintf(Message, "Pressure: %.1f\r\n", EnvParameters[2]);
	UARTDMA_Print(&huartdma2, Message);
}

/*
 * 		NAME=X\n	// Change name for X
 * 		NAME=?\n	// introduce yourself
 */
void UART_ParseNAME()
{
	char* ParsePointer = strtok(NULL, ","); // Get next string till token ',' or \0

	if(strlen(ParsePointer) > 0) // If string exists
	{
		if(strcmp(ParsePointer, "?") == 0) // If '?' is behind '='
		{
			sprintf(Message, "My name is %s\r\n", MyName); // Introduce yourself
		}
		else
		{
			strcpy(MyName, ParsePointer); // Change name for string passed in received message
			sprintf(Message, "Name changed to %s\r\n", MyName);
		}
	}
	else
	{
		// Error
		sprintf(Message, "Name cannot be empty!\r\n");
	}

	// Send back a message
	UARTDMA_Print(&huartdma2, Message);
}


/*
 * Parsing headers:
 * 		LED=1\n 	// LED On
 * 		LED=0\n 	// LED Off
 * 		ENV=X,Y,Z\n // X - temperature, Y - humidity, Z - pressure
 * 		NAME=X\n	// Change name for X
 * 		NAME=?\n	// introduce yourself
 */
void UART2_ParseLine(UARTDMA_HandleTypeDef *huartdma)
{
	char BufferReceive[64];

	if(!UARTDMA_GetLineFromReceiveBuffer(huartdma, BufferReceive))
	{
		// Header
		char* ParsePointer = strtok(BufferReceive, "="); // LED\0   1\0
		// ParsePointer == LED\0

	  if(strcmp(ParsePointer, "LED") == 0)
	  {
		  UART_ParseLED();
	  }
	  else if(strcmp(ParsePointer, "ENV") == 0)
	  {
		  UART_ParseENV();
	  }
	  else if(strcmp(ParsePointer, "NAME") == 0)
	  {
		  UART_ParseNAME();
	  }
	  else if(strcmp(ParsePointer, "CZAS") == 0)
	  {
		  MessageLen = sprintf((char*)Message, "Data: %02d.%02d.20%02d Godzina: %02d:%02d:%02d:%02d\r\n",
	 			  	  	  RtcDate.Date, RtcDate.Month, RtcDate.Year, RtcTime.Hours, RtcTime.Minutes, RtcTime.Seconds, RtcDate.WeekDay);
		  UARTDMA_Print(&huartdma2, (char*)Message);
	  }
	  else if(strcmp(ParsePointer, "SETDT") == 0)
	  {
		 weryfikujDate(ParsePointer);
	  }
	  else if(strcmp(ParsePointer, "SETTI") == 0)
	  {
		 weryfikujTime(ParsePointer);
	  }


	}
}

void UART1_ParseLine(UARTDMA_HandleTypeDef *huartdma)
{
	char BufferReceive[64];

	if(!UARTDMA_GetLineFromReceiveBuffer(huartdma, BufferReceive))
	{
		// Header
		char* ParsePointer = strtok(BufferReceive, "=");


	  if(strcmp(ParsePointer, "U2") == 0)
	  {
		  char* ParsePointer = strtok(NULL, ",");
		  UARTDMA_Print(&huartdma2, ParsePointer); // Print message
	  }

	}
}

void weryfikujDate(char bufor[120])
{
    uint8_t error1 = 1;
    uint8_t error2 = 1;
    uint8_t dd;
    uint8_t mm;
    uint8_t yy;

    char *tmp1;
    uint8_t iloscRozkazow = 0;
    char *tablica[20]={"\0"};

while ( bufor = strtok(NULL, ","))
		  {
            tmp1 = bufor;
            char polecenie[15]={"/0"};
            char rozkaz[15]={"/0"};
            uint8_t pozycja;
            pozycja = strcspn(tmp1,":");
            strncpy(polecenie,tmp1,pozycja);
            strcpy(rozkaz,tmp1+pozycja+1);

            char ctmp[15] = {"/0"};
            for (int j=0; j <= IloscPolecenData; j++ )
            {
               strcpy(ctmp, &poleceniaDATA[j]);
               char ctmp2[10] = {"/0"};
               if ( strcmp(ctmp, polecenie) == 0)
               {
                    for ( int i=0; poleceniaDATA[j].rozkaz[0][i]; i++ )
                    {
                        strcpy(ctmp2, poleceniaDATA[j].rozkaz[0][i]);
                        if ( strcmp(ctmp2, rozkaz) == 0)
                        {
                            error2 = 0;
                            tablica[iloscRozkazow]=tmp1;
                            iloscRozkazow++;
                            break;
                        }
                        else
                        {
                            char test[]={"\0"};
                            char zakres[10]={"\0"};
                            char z_od[10]={"\0"};
                            char z_do[10]={"\0"};
                            int i_rozkaz = 0;
                            strncpy(test,ctmp2,1);
                            error2 = 0;
                            if ( test[0] == 'd')
                            {
                                for (int i=0; rozkaz[i]; i++)
                                {
                                    if ( rozkaz[i] < '0' || rozkaz[i] > '9')
                                    {
                                        error2 = 1;
                                    }
                                }
                                if ( error2 == 0)
                                {
                                    strcpy(zakres,ctmp2+1);
                                    pozycja = strcspn(zakres,"-");
                                    strncpy(z_od,zakres,pozycja);
                                    strcpy(z_do,zakres+pozycja+1);
                                    i_rozkaz = atoi( rozkaz );
                                    if (atoi(rozkaz)<atoi(z_od) || atoi(rozkaz)>atoi(z_do))
                                    {
                                        error2 = 1;
                                    }
                                    else
                                    {
                                        tablica[iloscRozkazow]=tmp1;
                                        iloscRozkazow++;
                                    }
                                }
                            }
                            else if ( test[0] == 't')
                            {
                                for (int i=0; rozkaz[i]; i++)
                                {
                                    if ( rozkaz[i] < '0' || rozkaz[i] > '1')
                                    {
                                        error2 = 1;
                                    }
                                    if (i != 6) error2 = 1; else error2 = 0;
                                }
                                if (error2==0)
                                {
                                    tablica[iloscRozkazow]=tmp1;
                                    iloscRozkazow++;
                               }
                            }
                            else
                            {
                                error2 = 1;
                            }
                        }
                    }
                    if ( error2 == 1 )
                    {
                        //printf( "Bledna wartosc: %s dla polecenia %s (%s)\n", rozkaz, polecenie, tmp1 );
                    }
                    error1 = 0;
                    break;
               }
               else
               {
                    error1 = 1;
               }
            }
            if ( error1 == 1)
            {
                //printf( "Bledne polecenie: %s (%s) \n", rozkaz, tmp1 );
                break;
            }
        }
            if ( error1 == 0 && error2 == 0)
    {
        //printf( "Weryfikacja UDANA!!!!\n");
        for (uint8_t x=0; tablica[x]; x++)
        {
            char polecenie[15]={"/0"};
            char rozkaz[15]={"/0"};
            uint8_t pozycja;
            pozycja = strcspn(tablica[x],":");
            strncpy(polecenie,tablica[x],pozycja);
            strcpy(rozkaz,tablica[x]+pozycja+1);
            if ( strcmp(polecenie, "ROK" ) == 0 )
            {
            	yy = atoi(rozkaz);
            }
            if ( strcmp(polecenie, "MC" ) == 0 )
            {
            	mm = atoi(rozkaz);
            }
            if ( strcmp(polecenie, "DZ" ) == 0 )
            {
            	dd = atoi(rozkaz);
            }
        }
		error1 = 0;
		uint16_t yy2k = yy + 2000;
		if((dd>30) && (mm==4 || mm==6 || mm==9 || mm==11))
			error1 = 1;

		if((dd>29) && (mm==2))
			error1 = 1;

		if(dd==29 && mm==2)
		{
			if (yy2k%400==0 ||(yy2k%4==0 && yy2k%100!=0))
				 error1 = 0;
			else error1 = 1;
		}

		if (error1 == 0)
		{
			RtcDate.Year=yy;
			RtcDate.Month=mm;
			RtcDate.Date=dd;
			HAL_RTC_SetDate(&hrtc, &RtcDate, RTC_FORMAT_BIN);
		}
		//RtcTime.Hours=10;
		//RtcTime.Minutes=10;
		//RtcTime.Seconds=50;


	//	HAL_RTC_SetTime(&hrtc, &RtcTime, RTC_FORMAT_BIN);


		//printf( "Poprawny rozkazy do wykonania: %s\n", tablica[x] );

	// to do rozpoznania !!!
	//for (int j=0; tablica; j++)
	//   free(tablica[j]);

    }
    else
    {
        //printf( "Weryfikacja NIEUDANA!!!!\n");
    }


}

void weryfikujTime(char bufor[120])
{
    uint8_t error1 = 1;
    uint8_t error2 = 1;
    uint8_t gg;
    uint8_t mm;
    uint8_t ss;

    char *tmp1;
    uint8_t iloscRozkazow = 0;
    char *tablica[20]={"\0"};

while ( bufor = strtok(NULL, ","))
		  {
            tmp1 = bufor;
            char polecenie[15]={"/0"};
            char rozkaz[15]={"/0"};
            uint8_t pozycja;
            pozycja = strcspn(tmp1,":");
            strncpy(polecenie,tmp1,pozycja);
            strcpy(rozkaz,tmp1+pozycja+1);

            char ctmp[15] = {"/0"};
            for (int j=0; j <= IloscPolecenCzas; j++ )
            {
               strcpy(ctmp, &poleceniaTIME[j]);
               char ctmp2[10] = {"/0"};
               if ( strcmp(ctmp, polecenie) == 0)
               {
                    for ( int i=0; poleceniaTIME[j].rozkaz[0][i]; i++ )
                    {
                        strcpy(ctmp2, poleceniaTIME[j].rozkaz[0][i]);
                        if ( strcmp(ctmp2, rozkaz) == 0)
                        {
                            error2 = 0;
                            tablica[iloscRozkazow]=tmp1;
                            iloscRozkazow++;
                            break;
                        }
                        else
                        {
                            char test[]={"\0"};
                            char zakres[10]={"\0"};
                            char z_od[10]={"\0"};
                            char z_do[10]={"\0"};
                            int i_rozkaz = 0;
                            strncpy(test,ctmp2,1);
                            error2 = 0;
                            if ( test[0] == 'd')
                            {
                                for (int i=0; rozkaz[i]; i++)
                                {
                                    if ( rozkaz[i] < '0' || rozkaz[i] > '9')
                                    {
                                        error2 = 1;
                                    }
                                }
                                if ( error2 == 0)
                                {
                                    strcpy(zakres,ctmp2+1);
                                    pozycja = strcspn(zakres,"-");
                                    strncpy(z_od,zakres,pozycja);
                                    strcpy(z_do,zakres+pozycja+1);
                                    i_rozkaz = atoi( rozkaz );
                                    if (atoi(rozkaz)<atoi(z_od) || atoi(rozkaz)>atoi(z_do))
                                    {
                                        error2 = 1;
                                    }
                                    else
                                    {
                                        tablica[iloscRozkazow]=tmp1;
                                        iloscRozkazow++;
                                    }
                                }
                            }
                            else if ( test[0] == 't')
                            {
                                for (int i=0; rozkaz[i]; i++)
                                {
                                    if ( rozkaz[i] < '0' || rozkaz[i] > '1')
                                    {
                                        error2 = 1;
                                    }
                                    if (i != 6) error2 = 1; else error2 = 0;
                                }
                                if (error2==0)
                                {
                                    tablica[iloscRozkazow]=tmp1;
                                    iloscRozkazow++;
                               }
                            }
                            else
                            {
                                error2 = 1;
                            }
                        }
                    }
                    if ( error2 == 1 )
                    {
                        //printf( "Bledna wartosc: %s dla polecenia %s (%s)\n", rozkaz, polecenie, tmp1 );
                    }
                    error1 = 0;
                    break;
               }
               else
               {
                    error1 = 1;
               }
            }
            if ( error1 == 1)
            {
                //printf( "Bledne polecenie: %s (%s) \n", rozkaz, tmp1 );
                break;
            }
        }
            if ( error1 == 0 && error2 == 0)
    {
        //printf( "Weryfikacja UDANA!!!!\n");
        for (uint8_t x=0; tablica[x]; x++)
        {
            char polecenie[15]={"/0"};
            char rozkaz[15]={"/0"};
            uint8_t pozycja;
            pozycja = strcspn(tablica[x],":");
            strncpy(polecenie,tablica[x],pozycja);
            strcpy(rozkaz,tablica[x]+pozycja+1);
            if ( strcmp(polecenie, "GG" ) == 0 )
            {
            	gg = atoi(rozkaz);
            }
            if ( strcmp(polecenie, "MM" ) == 0 )
            {
            	mm = atoi(rozkaz);
            }
            if ( strcmp(polecenie, "SS" ) == 0 )
            {
            	ss = atoi(rozkaz);
            }
        }
		error1 = 0;

		RtcTime.Hours=gg;
		RtcTime.Minutes=mm;
		RtcTime.Seconds=ss;
		HAL_RTC_SetTime(&hrtc, &RtcTime, RTC_FORMAT_BIN);


		//printf( "Poprawny rozkazy do wykonania: %s\n", tablica[x] );

	// to do rozpoznania !!!
	//for (int j=0; tablica; j++)
	//   free(tablica[j]);

    }
    else
    {
        //printf( "Weryfikacja NIEUDANA!!!!\n");
    }


}

